# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 11:42:27 2020

@author: arthu
"""

"""produce a file containing the commands to use ripser in /scripts from ubuntu terminal (or similar)
    you need to then run the following command in your terminal
    
    dos2unix run.sh
    ./run.sh
    
    Warning : the dos2unix is necessary since the file generated from this script is not encoded correctly
    for ubuntu shell, so unexpected characters are added to the file extension without this pre-treatment
"""

# ripser parameters
dataset_name = 'sphere_3_192'


def generateRunRipser():
    output = open('run.sh', 'w')

    # setup automated shell script file
    output.write('#!/bin/sh')

    # write commands
    output.write('\n')
    output.write(generateCommand(dataset_name))

    output.close()


def generateCommand(dataset, maxdim=2, threshold=None):
    path_to_input = '../data/{}.lower_distance_matrix'.format(dataset)
    path_to_output = '../output/{}_ripser.txt'.format(dataset)

    if threshold is None:
        return '../tools/ripser/ripser --dim {} {} > {}'.format(maxdim,
                                                                path_to_input,
                                                                path_to_output)
    else:
        return '../tools/ripser/ripser --dim {} --threshold {} {} > {}'.format(maxdim,
                                                                               threshold,
                                                                               path_to_input,
                                                                               path_to_output)


generateRunRipser()

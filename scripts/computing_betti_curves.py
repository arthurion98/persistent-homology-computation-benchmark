import numpy as np
import ripser
from tadasets import dsphere, torus
from utility import compute_distance_matrix, full_matrix_to_lower_matrix_txt
from statistics import betti_curves

n_datasets = 100
size_datasets = 1000
d_sphere = 3

max_dim = 2
max_threshold_torus = 2
max_threshold_sphere = 2
dt = 0.00001

bcs_0_torus_1000 = []
bcs_1_torus_1000 = []
bcs_2_torus_1000 = []

bcs_0_sphere_1000 = []
bcs_1_sphere_1000 = []
bcs_2_sphere_1000 = []

bcs_0_torus_100 = []
bcs_1_torus_100 = []
bcs_2_torus_100 = []

bcs_0_sphere_100 = []
bcs_1_sphere_100 = []
bcs_2_sphere_100 = []

# generate n_datasets of size datasets points sampled on a torus and d-unit-sphere

for s in range(n_datasets):
    # sample points
    np.random.seed(s)
    data_torus = torus(n=size_datasets, c=0.66, a=0.33)

    np.random.seed(s)
    data_sphere = dsphere(n=size_datasets, d=3, r=1)

    # compute distance matrix
    distance_matrix_torus = compute_distance_matrix(data_torus)
    distance_matrix_sphere = compute_distance_matrix(data_sphere)

    # compute persistence
    persistence_ripser_torus_1000 = ripser.ripser(distance_matrix_torus, maxdim=max_dim, distance_matrix=True)
    persistence_ripser_sphere_1000 = ripser.ripser(distance_matrix_sphere, maxdim=max_dim, distance_matrix=True)
    persistence_ripser_torus_100 = ripser.ripser(distance_matrix_torus[:100, :][:, :100], maxdim=max_dim, distance_matrix=True)
    persistence_ripser_sphere_100 = ripser.ripser(distance_matrix_sphere[:100, :][:, :100], maxdim=max_dim, distance_matrix=True)

    # compute betti curves
    bcs_torus_1000 = betti_curves(persistence_ripser_torus_1000, dt, max_threshold_torus)
    bcs_0_torus_1000.append(bcs_torus_1000[0])
    bcs_1_torus_1000.append(bcs_torus_1000[1])
    bcs_2_torus_1000.append(bcs_torus_1000[2])

    bcs_sphere_1000 = betti_curves(persistence_ripser_sphere_1000, dt, max_threshold_sphere)
    bcs_0_sphere_1000.append(bcs_sphere_1000[0])
    bcs_1_sphere_1000.append(bcs_sphere_1000[1])
    bcs_2_sphere_1000.append(bcs_sphere_1000[2])

    bcs_torus_100 = betti_curves(persistence_ripser_torus_100, dt, max_threshold_torus)
    bcs_0_torus_100.append(bcs_torus_100[0])
    bcs_1_torus_100.append(bcs_torus_100[1])
    bcs_2_torus_100.append(bcs_torus_100[2])

    bcs_sphere_100 = betti_curves(persistence_ripser_sphere_100, dt, max_threshold_sphere)
    bcs_0_sphere_100.append(bcs_sphere_100[0])
    bcs_1_sphere_100.append(bcs_sphere_100[1])
    bcs_2_sphere_100.append(bcs_sphere_100[2])

    if s == 0:
        # write the first results and datasets to file
        full_matrix_to_lower_matrix_txt(distance_matrix_torus, '../data/torus_1000_s_{}.lower_distance_matrix'.format(s))
        full_matrix_to_lower_matrix_txt(distance_matrix_sphere, '../data/sphere_1000_s_{}.lower_distance_matrix'.format(s))

        np.save('../data/torus_1000_s_{}_per'.format(s), persistence_ripser_torus_1000)
        np.save('../data/sphere_1000_s_{}_per'.format(s), persistence_ripser_sphere_1000)
        np.save('../data/torus_100_s_{}_per'.format(s), persistence_ripser_torus_100)
        np.save('../data/sphere_100_s_{}_per'.format(s), persistence_ripser_sphere_100)

        np.save('../data/torus_1000_s_{}_bcs'.format(s), bcs_torus_1000)
        np.save('../data/sphere_1000_s_{}_bcs'.format(s), bcs_sphere_1000)
        np.save('../data/torus_100_s_{}_bcs'.format(s), bcs_torus_100)
        np.save('../data/sphere_100_s_{}_bcs'.format(s), bcs_sphere_100)


bmc_0_torus_1000 = np.mean(np.array(bcs_0_torus_1000), axis=0)
bmc_1_torus_1000 = np.mean(np.array(bcs_1_torus_1000), axis=0)
bmc_2_torus_1000 = np.mean(np.array(bcs_2_torus_1000), axis=0)

std_bmc_0_torus_1000 = np.std(np.array(bcs_0_torus_1000), axis=0)
std_bmc_1_torus_1000 = np.std(np.array(bcs_1_torus_1000), axis=0)
std_bmc_2_torus_1000 = np.std(np.array(bcs_2_torus_1000), axis=0)

np.save('../data/torus_1000_bmc', np.array([bmc_0_torus_1000, bmc_1_torus_1000, bmc_2_torus_1000]))
np.save('../data/torus_1000_std_bmc', np.array([std_bmc_0_torus_1000, std_bmc_1_torus_1000, std_bmc_2_torus_1000]))

bmc_0_sphere_1000 = np.mean(np.array(bcs_0_sphere_1000), axis=0)
bmc_1_sphere_1000 = np.mean(np.array(bcs_1_sphere_1000), axis=0)
bmc_2_sphere_1000 = np.mean(np.array(bcs_2_sphere_1000), axis=0)

std_bmc_0_sphere_1000 = np.std(np.array(bcs_0_sphere_1000), axis=0)
std_bmc_1_sphere_1000 = np.std(np.array(bcs_1_sphere_1000), axis=0)
std_bmc_2_sphere_1000 = np.std(np.array(bcs_2_sphere_1000), axis=0)

np.save('../data/sphere_1000_bmc', np.array([bmc_0_sphere_1000, bmc_1_sphere_1000, bmc_2_sphere_1000]))
np.save('../data/sphere_1000_std_bmc', np.array([std_bmc_0_sphere_1000, std_bmc_1_sphere_1000, std_bmc_2_sphere_1000]))

bmc_0_torus_100 = np.mean(np.array(bcs_0_torus_100), axis=0)
bmc_1_torus_100 = np.mean(np.array(bcs_1_torus_100), axis=0)
bmc_2_torus_100 = np.mean(np.array(bcs_2_torus_100), axis=0)

std_bmc_0_torus_100 = np.std(np.array(bcs_0_torus_100), axis=0)
std_bmc_1_torus_100 = np.std(np.array(bcs_1_torus_100), axis=0)
std_bmc_2_torus_100 = np.std(np.array(bcs_2_torus_100), axis=0)

np.save('../data/torus_100_bmc', np.array([bmc_0_torus_100, bmc_1_torus_100, bmc_2_torus_100]))
np.save('../data/torus_100_std_bmc', np.array([std_bmc_0_torus_100, std_bmc_1_torus_100, std_bmc_2_torus_100]))

bmc_0_sphere_100 = np.mean(np.array(bcs_0_sphere_100), axis=0)
bmc_1_sphere_100 = np.mean(np.array(bcs_1_sphere_100), axis=0)
bmc_2_sphere_100 = np.mean(np.array(bcs_2_sphere_100), axis=0)

std_bmc_0_sphere_100 = np.std(np.array(bcs_0_sphere_100), axis=0)
std_bmc_1_sphere_100 = np.std(np.array(bcs_1_sphere_100), axis=0)
std_bmc_2_sphere_100 = np.std(np.array(bcs_2_sphere_100), axis=0)

np.save('../data/sphere_100_bmc', np.array([bmc_0_sphere_100, bmc_1_sphere_100, bmc_2_sphere_100]))
np.save('../data/sphere_100_std_bmc', np.array([std_bmc_0_sphere_100, std_bmc_1_sphere_100, std_bmc_2_sphere_100]))
# dipha parameters
dataset_name = 'sphere_3_192'


def generateRunDipha():
    output = open('run.sh', 'w')

    # setup automated shell script file
    output.write('#!/bin/sh')

    # write commands
    output.write('\n')
    output.write(generateCommand(dataset_name))

    output.close()


def generateCommand(dataset, maxdim=3):
    path_to_input = '../data/{}.dipha'.format(dataset)
    path_to_output = '../output/{}_dipha.dipha'.format(dataset)

    return '../tools/dipha/dipha --upper_dim {} {} {}'.format(maxdim,
                                                                  path_to_input,
                                                                  path_to_output)


generateRunDipha()

import numpy as np
import utility
import struct


def convert_to_input_perseus(matrix, name, maxdim, N=1000):
    """create the input distance matrix file for perseus.
        :param matrix: full distance matrix
        :param name: path where the input txt file will be saved
        :param maxdim: maximum dimension of the Vietoris-Rips complex
        :param N: number of discrete thresholds for the filtration
        :return: none
    """
    n = len(matrix)
    m = max(matrix.ravel())

    output = open(name, 'w')
    output.write('{}\n'.format(n))
    output.write('0 {} {} {}\n'.format(m/N, N+1, maxdim))
    for i in range(n):
        for j in range(n):
            output.write('{}'.format(matrix[i, j]))
            if j < n - 1:  # last character
                output.write(' ')
        output.write('\n')
    output.close()


def convert_to_input_dipha(matrix, name):
    """create the input distance matrix file for dipha.
            :param matrix: full distance matrix
            :param name: path where the input binary file will be saved
            :return: none
        """
    n = len(matrix)

    output = open(name, 'wb')
    output.write((8067171840).to_bytes(8, byteorder='little'))
    output.write((7).to_bytes(8, byteorder='little'))
    output.write(n.to_bytes(8, byteorder='little'))
    for i in range(n):
        for j in range(n):
            output.write(struct.pack('<d', matrix[i, j]))
    output.close()


def read_dipha_output(name):
    """extract dipha output in ripser.py format, i.e. a three dimensional array ordered
        by dimension, pair, birth/death.
        :param name: path where the output is located
        :return: float array [dim][pair][birth/death]
    """
    file = open(name, 'rb')

    byte = file.read(8)
    dipha_identifier = int.from_bytes(byte, byteorder='little')

    byte = file.read(8)
    diagram_identifier = int.from_bytes(byte, byteorder='little')

    if dipha_identifier != 8067171840 or diagram_identifier != 2:
        raise TypeError

    byte = file.read(8)
    num_pairs = int.from_bytes(byte, byteorder='little')

    dims = []
    birth = []
    death = []
    for i in range(num_pairs):
        byte = file.read(8)
        dims.append(int.from_bytes(byte, byteorder='little', signed=True))
        byte = file.read(8)
        birth.append(struct.unpack('<d', byte))
        byte = file.read(8)
        death.append(struct.unpack('<d', byte))

    diagrams = []
    old_dim = -1
    buffer = None
    for i in range(num_pairs):
        if dims[i] >= 0:  # misses the (0,1) dim -1 encoded which probably corresponds to (0,inf) dim 0
            if dims[i] != old_dim:
                if old_dim == 0:  # correct for this missing dim -1 element
                    buffer.append((0, np.infty))
                if buffer is not None:
                    diagrams.append(buffer)
                buffer = []
                old_dim = dims[i]
            buffer.append((2*float(birth[i][0]), 2*float(death[i][0])))  # off by a factor 2, don't know why correcting it here
    if buffer is not None:
        diagrams.append(buffer)

    return diagrams


def read_ripser_output(name):
    """extract ripser output in ripser.py format, i.e. a three dimensional array ordered
        :param name: path where the output is located
        :return: float array [dim][pair][birth/death]
    """
    diagrams = []
    buffer = None
    dim = -1  # allow to start at dimension 0

    file = open(name, 'r')
    lines = file.readlines()
    for line in lines:
        if line[0] == 'p':  # persistence dimension line start with a 'p'
            dim = dim + 1
            if buffer is not None:
                diagrams.append(buffer)
            buffer = []
        if line[0] == ' ':  # data line start with a ' '
            data = line.split(',')
            start_d = data[0]
            start_d = float(
                start_d[2:])  # get the start of the persistent element of dimension dim by removing the starting '['

            end_d = data[1]
            end_d = end_d[0:-2]  # get the end of the persistent element of dimension dim by removing the ending ')'
            if end_d[0] == ' ':  # replace ' ' (blank) by the max_threshold + 1
                # ( +1 is just to get over the max_threshold) evaluated since it represents infinity
                end_d = np.infty
            end_d = float(end_d)

            buffer.append((start_d, end_d))
    if buffer is not None:
        diagrams.append(buffer)

    return diagrams


# import sphere_3_192 dataset (distance matrix)
#full_matrix = utility.lower_matrix_txt_to_full_matrix('../data/sphere_3_192.lower_distance_matrix', 192)
#convert_to_input_perseus(full_matrix, '../data/sphere_3_192.perseus', 2)
#convert_to_input_dipha(full_matrix, '../data/sphere_3_192.dipha')
#print(read_dipha_output('../output/sphere_3_192_dipha.dipha')[0])
#print(read_ripser_output('../output/sphere_3_192_ripser.txt')[0])



import numpy as np


def betti_curves(persistence, dt, max_threshold):
    """sample from a ripser output file the betti curves, return the betti curve
        values at each threshold
        :param persistence: persistence from ripser.py
        :param dt: threshold step used for sampling (start at threshold = 0)
        :param max_threshold: max threshold value for sampling
        :return: list of the sampled values [dim, threshold]
    """
    diagrams = persistence['dgms']
    dim = len(diagrams) - 1
    start = [[s for (s, e) in diagrams[d]] for d in range(0, dim + 1)]
    end = [[min(e, max_threshold) for (s, e) in diagrams[d]] for d in range(0, dim + 1)]

    nmax = int(max_threshold / dt)  # max number of multiples of dt
    betti_curves = np.zeros(((dim + 1), nmax + 1))

    for d in range(0, dim + 1):
        for k in range(0, len(start[d])):
            lowerbound = int(np.floor(start[d][k] / dt))
            upperbound = int(np.ceil(end[d][k] / dt))
            betti_curves[d][lowerbound:upperbound] = betti_curves[d][lowerbound:upperbound] + 1

    return betti_curves


def persistence_landscapes(persistence, dt, max_threshold):
    """sample from a ripser output file the persistence landscapes, return the persistence landscapes
        values at each threshold
        :param persistence: persistence from ripser.py
        :param dt: threshold step used for sampling (start at threshold = 0)
        :param max_threshold: max threshold value for sampling
        :return: list of the sampled values [dim, landscape, threshold]
    """
    #TODO: use np for min max comparison on a + t, b - t to increase perf
    diagrams = persistence['dgms']
    dim = len(diagrams) - 1
    start = [[s for (s, e) in diagrams[d]] for d in range(0, dim + 1)]
    end = [[min(e, max_threshold) for (s, e) in diagrams[d]] for d in range(0, dim + 1)]
    ts = np.linspace(0, max_threshold, (int(max_threshold / dt) + 1))

    persistence_landscapes = []
    for d in range(0, dim + 1):
        sorted_fs = np.sort(np.array([[max(0, min(a + t, b - t)) for t in ts] for (a, b) in zip(start[d], end[d])]), axis=0)
        # need to sort in reverse order so flip the sorted_fs
        if len(sorted_fs) > 1:
            sorted_fs = sorted_fs[::-1, :]
        persistence_landscapes.append(sorted_fs)

    return persistence_landscapes




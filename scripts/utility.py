import numpy as np


def compute_distance_matrix(data):
    """Compute the distance matrix using euclidean norm between points in a dataset.
        :param data: list of points in the form of vector
        :return: distance matrix of size size_dataset x size_dataset
    """
    size_dataset = len(data)
    distance_matrix = np.zeros((size_dataset, size_dataset))
    for i in range(1, size_dataset):
        for j in range(0, i):
            distance_matrix[i, j] = np.linalg.norm(data[i] - data[j])

    distance_matrix = distance_matrix + distance_matrix.T
    return distance_matrix


def full_matrix_to_lower_matrix_txt(matrix, name):
    """create a lower-distance matrix under a text file following the convention:
        ',' between values, line by line with a '\n'.
        :param matrix: full matrix to be converted
        :param name: path where the txt file will be saved
        :return: none
    """
    output = open(name, 'w')
    output.write('\n')
    n = len(matrix)
    for i in range(1, n):
        for j in range(0, i):
            output.write('{}'.format(matrix[i, j]))
            if j < n - 2:  # last character
                output.write(',')
        if i < n - 1:  # last character
            output.write('\n')
    output.close()


def lower_matrix_txt_to_full_matrix(name, n):
    """extract the full matrix from a (square) lower matrix in txt format,
        filling the diagonal with 0
        :param name: path to txt file for the lower matrix
        :param n: size of the full matrix
        :return: nxn full symmetric matrix
    """
    # generating adjacency matrix
    full_matrix = np.zeros((n, n))

    file = open(name, 'r')
    lines = file.readlines()
    i = 1  # 1 to N-1
    skip_first_line = True
    for line in lines:
        if skip_first_line:
            skip_first_line = False
        else:
            data = line.split(',')

            j = 0  # 0 to i-1
            for d in data:
                if d != '\n':  # last ',' separate a '\n'
                    full_matrix[i, j] = float(d)
                    j += 1
            i += 1

    # symmetrize
    full_matrix = full_matrix.T + full_matrix

    return full_matrix


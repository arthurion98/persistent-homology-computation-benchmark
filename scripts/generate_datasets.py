import numpy as np
from tadasets import dsphere, torus
from utility import full_matrix_to_lower_matrix_txt

n_datasets = 100
size_datasets = 100
d_sphere = 3

# generate n_datasets of size datasets points sampled on a d-unit-sphere

for s in range(n_datasets):
    # sample points
    np.random.seed(s)
    data = torus(n=size_datasets, c=0.66, a=0.33) #dsphere(n=size_datasets, d=3, r=1)

    # compute distance matrix
    distance_matrix = np.zeros((size_datasets, size_datasets))
    for i in range(1, size_datasets):
        for j in range(0, i):
            distance_matrix[i, j] = np.linalg.norm(data[i]-data[j])

    distance_matrix = distance_matrix + distance_matrix.T

    # write to file
    full_matrix_to_lower_matrix_txt(distance_matrix, '../data/torus_{}_s_{}.lower_distance_matrix'.format(size_datasets, s))


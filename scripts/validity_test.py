import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import ripser
import gudhi
import persim
import utility
import converter
from gph import ripser_parallel

max_dim = 2


def compute_persistence():
    """compute persistence on sphere_3_192 up to dimension max_dim in Z/2Z for giotto-ph, ripser.py and gudhi.
        :return: none
    """

    # import sphere_3_192 dataset (distance matrix)
    full_matrix = utility.lower_matrix_txt_to_full_matrix('../data/sphere_3_192.lower_distance_matrix', 192)

    # compute persistence

    # giotto-ph
    persistence_giotto = ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)
    for dim, diag in enumerate(persistence_giotto['dgms']):
        np.save('../output/persistence_diagram/giotto_ph_dim={}'.format(dim), diag)

    # ripser.py
    persistence_ripser = ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)

    for dim, diag in enumerate(persistence_ripser['dgms']):
        np.save('../output/persistence_diagram/ripser_py_dim={}'.format(dim), diag)

    # gudhi
    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim+1)
    simplex_tree.compute_persistence(homology_coeff_field=2)

    for dim in range(max_dim+1):
        np.save('../output/persistence_diagram/gudhi_dim={}'.format(dim), simplex_tree.persistence_intervals_in_dimension(dim))


def convert_persistence():
    """convert persistence computations on sphere_3_192 up to dimension max_dim in Z/2Z for ripser and dipha.
        :return: none
    """
    # ripser
    diagrams = converter.read_ripser_output('../output/sphere_3_192_ripser.txt')
    for dim, diag in enumerate(diagrams):
        np.save('../output/persistence_diagram/ripser_dim={}'.format(dim), diag)

    # dipha
    diagrams = converter.read_dipha_output('../output/sphere_3_192_dipha.dipha')
    for dim, diag in enumerate(diagrams):
        np.save('../output/persistence_diagram/dipha_dim={}'.format(dim), diag)


def compute_bottleneck_distance(tool_labels):
    """compute bottleneck between persistence diagrams for each dimension
        :tool_labels: list of the reference string used to save the output of each PH computation tool
        :return: list (from dimension 0 to max_dim) of bottleneck distance matrix
    """
    n = len(tool_labels)
    bottleneck_matrices = []
    #wasserstein_matrices = []
    for dim in range(max_dim+1):
        bottleneck_distance_matrix = np.zeros((n, n))
        #wasserstein_distance_matrix = np.zeros((n, n))

        # pre-load the diagrams
        diagrams = [np.load('../output/persistence_diagram/{}_dim={}.npy'.format(tool, dim)) for tool in tool_labels]

        persim.plot_diagrams(diagrams, labels=tool_labels)
        plt.show()

        for i in range(1, n):
            for j in range(0, i):
                bottleneck_distance_matrix[i, j] = gudhi.bottleneck_distance(diagrams[i], diagrams[j])
                #wasserstein_distance_matrix[i, j] = gudhi.wasserstein.wasserstein_distance(diagrams[i], diagrams[j], order=1., internal_p=2.)

        # symmetrize
        bottleneck_distance_matrix = bottleneck_distance_matrix.T + bottleneck_distance_matrix
        #wasserstein_distance_matrix = wasserstein_distance_matrix.T + wasserstein_distance_matrix

        bottleneck_matrices.append(bottleneck_distance_matrix)
        #wasserstein_matrices.append(wasserstein_distance_matrix)

    return bottleneck_matrices


#compute_persistence()
#convert_persistence()

tool_labels = ['giotto_ph', 'ripser_py', 'gudhi', 'dipha', 'ripser', 'dionysus']
bms = compute_bottleneck_distance(tool_labels)
for bm in bms:
    mask = np.where(np.tril(np.ones((len(tool_labels), len(tool_labels))), k=-1) == 1, True, False)
    ax = sns.heatmap(bm, linewidth=0.5, xticklabels=tool_labels, yticklabels=tool_labels, mask=mask)
    plt.show()

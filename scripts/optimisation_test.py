import numpy as np
import os
import ripser
import gudhi
import dionysus as d  # need to comment out if you run it on windows
import utility
import ripser_link
import dipha_link
from scipy.spatial.distance import squareform
from gph import ripser_parallel

# parameters
max_dim = 2
full_matrix = utility.lower_matrix_txt_to_full_matrix('../data/sphere_3_192.lower_distance_matrix', 192)


def method_giotto_ph():
    # repeating ten times to get statistics
    ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)
    ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)
    ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)
    ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)
    ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)
    ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)
    ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)
    ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)
    ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)
    ripser_parallel(full_matrix, metric="precomputed", maxdim=max_dim, n_threads=1)


def method_ripser_py():
    # repeating ten times to get statistics
    ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)
    ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)
    ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)
    ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)
    ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)
    ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)
    ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)
    ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)
    ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)
    ripser.ripser(full_matrix, maxdim=max_dim, distance_matrix=True)


def method_gudhi():
    # repeating ten times to get statistics
    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim + 1)
    simplex_tree.compute_persistence(homology_coeff_field=2)

    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim + 1)
    simplex_tree.compute_persistence(homology_coeff_field=2)

    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim + 1)
    simplex_tree.compute_persistence(homology_coeff_field=2)

    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim + 1)
    simplex_tree.compute_persistence(homology_coeff_field=2)

    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim + 1)
    simplex_tree.compute_persistence(homology_coeff_field=2)

    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim + 1)
    simplex_tree.compute_persistence(homology_coeff_field=2)

    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim + 1)
    simplex_tree.compute_persistence(homology_coeff_field=2)

    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim + 1)
    simplex_tree.compute_persistence(homology_coeff_field=2)

    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim + 1)
    simplex_tree.compute_persistence(homology_coeff_field=2)

    rips_complex = gudhi.RipsComplex(distance_matrix=full_matrix, max_edge_length=np.inf)
    simplex_tree = rips_complex.create_simplex_tree(max_dimension=max_dim + 1)
    simplex_tree.compute_persistence(homology_coeff_field=2)


def method_ripser():
    # repeating ten times to get statistics
    ripser_link.generateRunRipser()
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')


def method_dipha():
    # repeating ten times to get statistics
    dipha_link.generateRunDipha()
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')
    os.system('./run.sh')


def method_dionysus():
    # repeating ten times to get statistics
    lower_dist = squareform(full_matrix)
    max_dist = max(full_matrix.ravel())

    f = d.fill_rips(lower_dist, max_dim + 1, max_dist)
    m = d.homology_persistence(f)

    f = d.fill_rips(lower_dist, max_dim + 1, max_dist)
    m = d.homology_persistence(f)

    f = d.fill_rips(lower_dist, max_dim + 1, max_dist)
    m = d.homology_persistence(f)

    f = d.fill_rips(lower_dist, max_dim + 1, max_dist)
    m = d.homology_persistence(f)

    f = d.fill_rips(lower_dist, max_dim + 1, max_dist)
    m = d.homology_persistence(f)

    f = d.fill_rips(lower_dist, max_dim + 1, max_dist)
    m = d.homology_persistence(f)

    f = d.fill_rips(lower_dist, max_dim + 1, max_dist)
    m = d.homology_persistence(f)

    f = d.fill_rips(lower_dist, max_dim + 1, max_dist)
    m = d.homology_persistence(f)

    f = d.fill_rips(lower_dist, max_dim + 1, max_dist)
    m = d.homology_persistence(f)

    f = d.fill_rips(lower_dist, max_dim + 1, max_dist)
    m = d.homology_persistence(f)


# profiling
prof = profile(method_giotto_ph)
prof()
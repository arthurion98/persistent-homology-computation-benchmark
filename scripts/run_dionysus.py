import numpy as np
import dionysus as d
import utility
from scipy.spatial.distance import squareform

max_dim = 2

# import sphere_3_192 dataset (distance matrix)
full_matrix = utility.lower_matrix_txt_to_full_matrix('../data/sphere_3_192.lower_distance_matrix', 192)
lower_dist = squareform(full_matrix)
max_dist = max(full_matrix.ravel())

# compute persistence
f = d.fill_rips(lower_dist, max_dim+1, max_dist)
m = d.homology_persistence(f)

diagrams = d.init_diagrams(m, f)

for dim, diag in enumerate(diagrams):
    if dim <= max_dim:  # last dimension has no boundaries, so we do not consider it
        np.save('../output/persistence_diagram/dionysus_dim={}'.format(dim), [(pt.birth, pt.death) for pt in diag])
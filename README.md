# Stochastic Approximation of Persistent Homology Computation

by Tâm NGUYEN

source code of the report used for the benchmark and the examples using the proposed algorithms.

requires:
- python 3.7+
- SciPy (https://scipy.org/)
- Ripser.py (https://ripser.scikit-tda.org/en/latest/)

further requirements (benchmark and datasets generation):
- TaDAsets (https://tadasets.scikit-tda.org/en/latest/)
- GUDHI (https://gudhi.inria.fr/)
- giotto-ph (https://github.com/giotto-ai/giotto-ph)
- Ripser (linux) (https://github.com/Ripser/ripser)
- dionysus 2 (linux) (https://mrzv.org/software/dionysus2/)
- DIPHA (linux) (https://github.com/DIPHA/dipha)
- line_profiler (https://pypi.org/project/line-profiler/)
- memory_profiler (https://pypi.org/project/memory-profiler/)

## Files

benchmark:
- generate_datasets.py (generate size 100 datasets)
- dipha_link.py (create a .sh file to run DIPHA)
- ripser_link.py (create a .sh file to run Ripser)
- run_dionysus.py (run dionysus)
- validity_test.py (run ripser.py, gudhi, giotto-ph; convert results of DIPHA, Ripser, dionysus; compute bottleneck distance)
- optimisation_test.py (time and memory profiling; need to change the line 155 : profile(method) to the method you want to profile)

stochastic approximation:
- computing_betti_curves.py (run computation for CMC on size 1000 datasets)
- Stochastic Approximation of Betti Curves - Sphere.ipynb (example on 3-sphere)
- Stochastic Approximation of Betti Curves - Torus.ipynb (example on torus)

utilities:
- converter.py (i/o converter for Perseus, DIPHA and Ripser to ripser.py format)
- statistics.py (Betti curves and Landscapes computation)
- utility.py (converter to/from numpy matrices to csv/text format)

## Use of the code

you may use any part of my code however you deem fit, as long as you credit
me and link to this repository.
